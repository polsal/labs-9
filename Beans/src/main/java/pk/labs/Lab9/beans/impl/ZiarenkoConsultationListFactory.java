/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.ConsultationListFactory;

/**
 *
 * @author Maciej
 */
public class ZiarenkoConsultationListFactory implements ConsultationListFactory{

    @Override
    public ConsultationList create() {
        return create(false);
    }

    @Override
    public ConsultationList create(boolean deserialize) {
        ConsultationList ziarenko_lista = null;        
            if (deserialize) {
                // TODO use deserilization
            } else {
                ziarenko_lista = new ZiarenkoConsultationList();        
            }
            return ziarenko_lista; 
    }

    @Override
    public void save(ConsultationList consultationList) {
    }
    
    public ZiarenkoConsultationListFactory() {
    }
}
