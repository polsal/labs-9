package pk.labs.Lab9;

import pk.labs.Lab9.beans.*;
import pk.labs.Lab9.beans.impl.*;

public class LabDescriptor {
    
    public static Class<? extends Term> termBean = ZiarenkoTerm.class;
    public static Class<? extends Consultation> consultationBean = ZiarenkoConsultation.class;
    public static Class<? extends ConsultationList> consultationListBean = ZiarenkoConsultationList.class;
    public static Class<? extends ConsultationListFactory> consultationListFactory = ZiarenkoConsultationListFactory.class;
    
}
